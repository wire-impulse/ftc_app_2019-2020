package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import static com.qualcomm.robotcore.hardware.DcMotorSimple.Direction.FORWARD;
import static com.qualcomm.robotcore.hardware.DcMotorSimple.Direction.REVERSE;

public class hardwareMecanum {
    /* Public OpMode members. */
    public DcMotor backArm = null;
    public DcMotor frontArm = null;

    public DcMotor motorBackCollector = null;
    public DcMotor motorFrontCollector = null;

    public DcMotor FL = null;
    public DcMotor FR = null;
    public DcMotor BL = null;
    public DcMotor BR = null;

    public CRServo servoFrontLeft  = null;
    public CRServo servoFrontRight = null;

    public Servo frontArmDoor = null;

    /* local OpMode members. */
    HardwareMap hwMap           =  null;
    private ElapsedTime period  = new ElapsedTime();

    /* Constructor */
    public hardwareMecanum(){

    }

    /* Initialize standard Hardware interfaces */
    public void init(HardwareMap ahwMap) {
        // Save reference to Hardware map
        hwMap = ahwMap;

        /**
         * Motors
         */
        //Get hardware for motors
        FL = hwMap.dcMotor.get("FL");
        BL = hwMap.dcMotor.get("BL");
        FR = hwMap.dcMotor.get("FR");
        BR = hwMap.dcMotor.get("BR");

        backArm = hwMap.dcMotor.get("backArm");
        frontArm = hwMap.dcMotor.get("frontArm");

        motorFrontCollector = hwMap.dcMotor.get("motorFrontCollector");
        motorBackCollector = hwMap.dcMotor.get("motorBackCollector");

        /**
         * Servo
         * CR & Servo
         */
        servoFrontLeft  = hwMap.crservo.get("servoFrontLeft");
        servoFrontRight = hwMap.crservo.get("servoFrontRight");

        frontArmDoor = hwMap.servo.get("frontArmDoor");

        //Set direction to all motors
        FL.setDirection(REVERSE);
        BL.setDirection(REVERSE);
        FR.setDirection(FORWARD);
        BR.setDirection(FORWARD);


        // Set all motors to run without encoders.
        FL.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        BL.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        FR.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        BR.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        backArm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontArm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        motorBackCollector.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontArmDoor.setPosition(-1.0);

        servoFrontRight.setDirection(REVERSE);
    }
}
